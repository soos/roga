package tk.salvian.roga.roga2;

import android.util.Log;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipInputStream;

public class RogaWebSocketClient extends WebSocketClient {

    private final String TAG = "Roga";

    private final String mLogin;
    private final String mPassword;

    public RogaWebSocketClient( Draft d , URI uri, String login, String password ) {
        super( uri, d );
        mLogin = login;
        mPassword = password;
        Log.d(TAG, "ws.Constructor");
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        Log.d(TAG, "ws.onOpen "+serverHandshake.getHttpStatusMessage()+" "+serverHandshake.getHttpStatus());
        JSONObject jobj = new JSONObject();
        try {
            jobj.put("Command", "login");
            jobj.put("Login", mLogin);
            jobj.put("Password", mPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String json = jobj.toString();
        Log.d(TAG, json);
        send(json);
    }

    @Override
    public void onMessage(String s) {
        Log.d(TAG, "ws.onMessage "+s);
        //send(s);
    }

    @Override
    public void onMessage( ByteBuffer blob ) {
        Log.d(TAG, "ws.onMessage "+blob.toString());

        Inflater inflater = new Inflater();
        byte[] header = {(byte)0x78,(byte)0x9c};
        byte[] data = blob.array();
        byte[] array = new byte[header.length + data.length];
        System.arraycopy(header, 0, array, 0, header.length);
        System.arraycopy(data, 0, array, header.length, data.length);
        inflater.setInput(array, 0, array.length);
        try {
            byte[] buffer = new byte[0];
            while (true) {
                byte[] part = new byte[1024];
                int len = inflater.inflate(part);
                if (len <= 0) break;
                byte[] newbuf = new byte[buffer.length + len];
                System.arraycopy(buffer, 0, newbuf, 0, buffer.length);
                System.arraycopy(part, 0, newbuf, buffer.length, len);
                buffer = newbuf;
            }
            inflater.end();
            String outputString = new String(buffer, 0, buffer.length, "UTF-8");
            Log.d(TAG, outputString);
        } catch (DataFormatException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        Log.d(TAG, "ws.onClose");
    }

    @Override
    public void onError(Exception e) {
        Log.e(TAG, "ws.onError "+e.getLocalizedMessage());
    }
}
