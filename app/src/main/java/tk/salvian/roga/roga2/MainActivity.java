package tk.salvian.roga.roga2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;

import java.net.URI;

public class MainActivity extends AppCompatActivity {

    private WebSocketClient mWs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText editTextLogin = (EditText) findViewById(R.id.editTextLogin);
        final EditText editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        final Button buttonConnect = (Button) findViewById(R.id.buttonConnect);
        assert buttonConnect != null;
        buttonConnect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mWs == null) {
                    //URI uri = URI.create("ws://echo.websocket.org");
                    //URI uri = URI.create("ws://salvian.tk:4444/");
                    //URI uri = URI.create("wss://rogalik.tatrix.org:49443/");
                    URI uri = URI.create("ws://rogalik.tatrix.org:49000/");
                    mWs = new RogaWebSocketClient(new Draft_17(), uri, editTextLogin.getText().toString(), editTextPassword.getText().toString());
                    mWs.connect();
                }
            }
        });

        final Button buttonDisconnect = (Button) findViewById(R.id.buttonDisconnect);
        assert buttonDisconnect != null;
        buttonDisconnect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mWs != null) {
                    mWs.close();
                    mWs = null;
                }
            }
        });
    }
}
